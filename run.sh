#!/bin/bash

$ cat cheapwatch 
#!/bin/sh

while true ; do
  clear
  printf "[%s] Output of %s:\n" "$(bash ~/.config/badtop/sys.sh)" "$*"
  ${SHELL-/bin/sh} -c "$*"
  sleep 1
done

$ ./cheapwatch ls --color

