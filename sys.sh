#!/bin/bash

tput setaf 3; cat ~/.config/badtop/figlet
tput sgr0
cat ~/.config/badtop/lines
tput setaf 30; cat ~/.config/badtop/welcome && whoami
tput sgr0
cat ~/.config/badtop/lines
tput setaf 2; free -m
tput sgr0
cat ~/.config/badtop/lines
tput setaf 75; date
tput sgr0
tput setaf 1; uptime
tput sgr0
cat ~/.config/badtop/lines
tput setaf 4; cat ~/.config/badtop/processes && ps -e | wc -l
tput sgr0
tput setaf 7; ps -au | less
tput sgr0
